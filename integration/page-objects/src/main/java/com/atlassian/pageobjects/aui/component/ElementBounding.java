package com.atlassian.pageobjects.aui.component;

import com.atlassian.pageobjects.elements.WebDriverElement;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

/**
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */
@Deprecated
public class ElementBounding {

    public enum BoundingPosition {
        LEFT,
        RIGHT;
    }

    private final WebDriverElement element;

    public ElementBounding(WebDriverElement element)
    {
        this.element = element;
    }

    public Point getLocation() {
        return element.asWebElement().getLocation();
    }

    public Dimension getSize()
    {
        return element.asWebElement().getSize();
    }

    public boolean contains(BoundingPosition position, ElementBounding bounding)
    {
        switch(position)
        {
            case LEFT:
                return this.getLocation().getX() <= bounding.getLocation().getX();
            case RIGHT:
                return this.getLocation().getX() + this.getSize().getWidth() >= bounding.getLocation().getX() + bounding.getSize().getWidth();
            default:
                throw new RuntimeException("Unknown BoundingPosition: " + position);
        }
    }

}
