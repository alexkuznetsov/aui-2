package com.atlassian.aui.test.contacts.entities;

import net.java.ao.schema.NotNull;
import net.java.ao.Preload;
import net.java.ao.RawEntity;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.PrimaryKey;

@Preload
public interface Contact extends RawEntity<Long>
{

    @AutoIncrement
    @NotNull
    @PrimaryKey ("ID")
    public long getId();

    public void setPos(int pos);

    public int getPos();

    public String getName();

    public void setName(final String name);

    public String getGroup();

    public void setGroup(final String group);

    public String getPhoneNumber();

    public void setPhoneNumber(final String number);

}
