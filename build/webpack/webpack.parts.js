const { BannerPlugin, DefinePlugin } = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const project = require('../../package.json');
const banner = require('../lib/banner');

const { NODE_ENV } = process.env;

exports.setAuiVersion = () => ({
    plugins: [
        new DefinePlugin({
            AUI_VERSION: JSON.stringify(project.version)
        })
    ]
});

exports.transpileJs = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/i,
                include,
                exclude,
                use: {
                    loader: 'babel-loader',
                    options,
                },
            },
        ]
    }
});

exports.extractCss = ({ include, exclude, options } = {}) => {
    const extractLESS = new ExtractTextPlugin(options);

    return {
        module: {
            rules: [
                {
                    test: /\.less$/i,
                    include,
                    exclude,

                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',

                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'less-loader',
                                options: {
                                    sourceMap: true
                                }
                            }
                        ]
                    })
                },
            ],
        },

        plugins: [
            extractLESS
        ],
    }
};

exports.loadFonts = ({ include, exclude, options } = {}) => {
    return {
        module: {
            rules: [
                {
                    test: /\.(ttf|eot|woff|woff2)$/,
                    include,
                    exclude,
                    use: {
                        loader: 'file-loader',
                        options,
                    },
                }
            ]
        }
    };
};

exports.loadImages = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif|svg)$/,
                include,
                exclude,
                use: {
                    loader: 'file-loader',
                    options,
                },
            },
        ],
    },
});

exports.loadSoy = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.soy$/,
                include,
                exclude,
                use: {
                    loader: '@atlassian/atlassian-soy-loader',
                    options,
                },
            },

            {
                test: require.resolve('@atlassian/soy-template-plugin-js/src/js/atlassian-deps.js'),

                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'atl_soy'
                    }
                ]
            },

            {
                test: require.resolve('@atlassian/soy-template-plugin-js/src/js/soyutils.js'),

                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'soy,soydata,goog'
                    }
                ]
            }
        ]
    },
});

exports.production = () => {
    if (NODE_ENV !== 'production') {
        return {};
    }

    return {
        devtool: 'source-map',

        plugins: [
            new DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(NODE_ENV)
                }
            }),

            new BannerPlugin({
                banner: banner,
                raw: true,
                entryOnly: true
            }),

            new UglifyJsPlugin({
                include: /\.min\.js$/,
                uglifyOptions: {
                    compress: {
                        warnings: false,
                        drop_console: true,
                        drop_debugger: true
                    },
                    mangle: false,
                    output: {
                        beautify: false,
                        comments: /^!!/
                    }
                },

                sourceMap: true,
                parallel: true,
                cache: true
            })
        ]
    }
};
