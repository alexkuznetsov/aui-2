const path = require('path');
const merge = require('webpack-merge');
const { DllPlugin } = require('webpack');
const DllBootstrapPlugin = require('./plugins/DllBootstrapPlugin');
const { outputDir, librarySkeleton } = require('./webpack.skeleton');

const auiLib = path.resolve('src', 'entry', 'aui.js');

module.exports = merge([
    librarySkeleton,

    {
        entry: {
            'aui': [ auiLib ],
            'aui.min': [ auiLib ],
        },

        externals: [
            {
                'jquery': 'jQuery',
            }
        ],

        output: {
            libraryTarget: 'var',
            library: '__AJS',
        },

        plugins: [
            new DllPlugin({
                path: path.resolve(outputDir, 'aui-manifest.json'),
                name: '__AJS'
            }),

            new DllBootstrapPlugin({
                'aui': auiLib
            }),
        ],
    }
]);
