const path = require('path');
const merge = require('webpack-merge');
const {DllReferencePlugin} = require('webpack');
const { outputDir, librarySkeleton } = require('./webpack.skeleton');

module.exports = merge([
    librarySkeleton,

    {
        entry: {
            'aui-css-deprecations': path.resolve('src', 'js', 'aui-css-deprecations.js'),
            'aui-css-deprecations.min': path.resolve('src', 'js', 'aui-css-deprecations.js'),
            'aui-datepicker': path.resolve('src', 'entry', 'aui-datepicker.js'),
            'aui-datepicker.min': path.resolve('src', 'entry', 'aui-datepicker.js'),
            'aui-experimental': path.resolve('src', 'entry', 'aui-experimental.js'),
            'aui-experimental.min': path.resolve('src', 'entry', 'aui-experimental.js'),
            'aui-header-async': path.resolve('src', 'js', 'aui-header-async.js'),
            'aui-header-async.min': path.resolve('src', 'js', 'aui-header-async.js'),
        },

        externals: [
            {
                'jquery': 'jQuery',
            }
        ],

        plugins: [
            new DllReferencePlugin({
                context: '.',
                manifest: path.resolve(outputDir, 'aui-manifest.json')
            }),
        ]
    }
]);
