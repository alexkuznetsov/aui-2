#!/bin/bash
#set -x
set -e

# Test we got the correct parameters for the command
PLUGIN_XML=$1
PLUGIN_POM=$2

if [ -z "${PLUGIN_XML}" ] || [ -z "${PLUGIN_POM}" ]; then
  echo "Usage: $(basename ${0}) [plugin.xml] [pom.xml]"
  exit 1
fi

if [ ! -f "${PLUGIN_XML}" ]; then
  echo "Could not find atlassian-plugin.xml: ${PLUGIN_XML}"
  exit 1
fi

if [ ! -f "${PLUGIN_POM}" ]; then
  echo "Could not find plugin pom.xml: ${PLUGIN_POM}"
  exit 1
fi

# Fail fast if we don't have the requisite dependencies
command -v xmlstarlet >/dev/null 2>&1 || { echo >&2 "Verifying the plugin XML requires xmlstarlet but it's not installed. Aborting."; exit 2; }

XMLSTARLET=$(command -v xmlstarlet)

EXTRA_MODULE_DIRS=$(${XMLSTARLET} sel --text \
                                      --template \
                                      --match '//*[local-name() = "plugin"][*[local-name() = "groupId"]="com.atlassian.maven.plugins"]/*[local-name() = "configuration"]/*[local-name() = "instructions"]/*[local-name() = "Atlassian-Scan-Folders"]' \
                                      --value-of "concat('$(dirname ${PLUGIN_XML})/', node())" \
                                      --output ' ' \
                                      ${PLUGIN_POM})
EXTRA_MODULES=$(find ${EXTRA_MODULE_DIRS} -type f -name '*.xml')
XML_FILES="${PLUGIN_XML} ${EXTRA_MODULES}"

# Make a temporary directory for the FIFO files
TMP=$(mktemp -d /tmp/auiplugin.XXXXXXX)
DEPS_DECLARED=${TMP}/deps_declared
DEPS_USED=${TMP}/deps_used
DUPE_DECLARATIONS=${TMP}/dupe_declarations
NESTED_NODE_MODULES=${TMP}/nested_node_modules

mkfifo ${DEPS_DECLARED}
mkfifo ${DEPS_USED}
mkfifo ${DUPE_DECLARATIONS}
mkfifo ${NESTED_NODE_MODULES}

retcode=0

##
# Ensure local (com.atlassian.auiplugin) dependencies are actually declared.
#
${XMLSTARLET} sel --template \
                  --match '//web-resource' \
                  --value-of '@key' --nl \
                  ${XML_FILES} \
    | sort --unique \
    > ${DEPS_DECLARED} &

${XMLSTARLET} sel --template \
                  --match "//web-resource/dependency[starts-with(., '${PACKAGE}:')]" \
                  --value-of . --nl \
                  ${XML_FILES} \
    | cut -d ':' -f 2 \
    | sort --unique \
    > ${DEPS_USED} &

while read undeclared_dep; do
   retcode=1
   echo Undeclared dependency: ${PACKAGE}:${undeclared_dep}
done < <(comm -23 ${DEPS_USED} ${DEPS_DECLARED})

##
# Ensure there are no duplicate web-resource declarations.
#
${XMLSTARLET} sel --template \
               --match '//web-resource[@key = following-sibling::web-resource/@key and not(@key = preceding-sibling::web-resource/@key)]' \
               --value-of '@key' --nl \
               ${XML_FILES} \
    | sort \
    > ${DUPE_DECLARATIONS} &

while read dupe_dep; do
    retcode=1
    echo Duplicate web-resource: ${dupe_dep}
done < ${DUPE_DECLARATIONS}

rm -rf ${TMP}
exit ${retcode}
