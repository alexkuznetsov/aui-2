'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');
var karma = require('../../lib/karma');

var testBuildTask = gat.load('test/build');
var opts = gat.opts();

module.exports = gulp.series(
    testBuildTask,
    function run (done) {
        karma(opts, done).start();
    }
);
