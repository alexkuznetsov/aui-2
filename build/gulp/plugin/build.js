'use strict';

var gulp = require('gulp');
var pluginDeps = require('./deps');
var pluginMavenInstall = require('./maven-install');
var pluginVerifyXml = require('./verify-plugin-xml');

module.exports = gulp.series(
    pluginDeps,
    pluginVerifyXml,
    pluginMavenInstall
);
