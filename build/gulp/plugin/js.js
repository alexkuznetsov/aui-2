'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpBabel = require('gulp-babel');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var isEs6 = require('../../lib/is-es6');
var libFilterPluginExcludeJs = require('../../lib/plugin-exclude-js');
var libTraceMap = require('../../lib/trace-map');
var libVersionReplace = require('../../lib/version-replace');

module.exports = function pluginJs () {
    var opts = gat.opts();
    var filterPluginExcludeJs = libFilterPluginExcludeJs();
    return galv.trace('src/js/aui*.js', {
        map: libTraceMap(opts.root)
    }).createStream()
        .pipe(libVersionReplace())
        .pipe(filterPluginExcludeJs)
        .pipe(gulpIf(isEs6, gulpBabel()))
        .pipe(galv.globalize())
        .pipe(gulpDebug({title: 'plugin/js'}))
        .pipe(gulp.dest('.tmp/plugin'));
};
