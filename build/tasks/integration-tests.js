var proc = require('child_process');

var INTEGRATION_PATH = 'integration/refapp';

module.exports = function(args, paths, cb) {
    var cmd = proc.spawn('mvn', [
        'verify',
        '-DskipAllPrompts=true',
        `-Djquery.version=${args.jquery || '1.8.3'}`,
    ], {
        cwd: INTEGRATION_PATH
    });

    cmd.stdout.on('data', function(data) {
        console.log(data.toString());
    });

    cmd.on('close', function(code) {
        cb(code);
    });
};
