const LessPluginNpmImport = require('less-plugin-npm-import');

const lessNpmImportPlugin = new LessPluginNpmImport({
    prefix: '~'
});

module.exports = lessNpmImportPlugin;
