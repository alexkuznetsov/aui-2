'use strict';

var proc = require('child_process');

var soyJarPath = require.resolve('@atlassian/soy-template-plugin-js/src/jar/atlassian-soy-cli-support.jar');

function toKeyVal (obj) {
    var parts = [];

    for (var a in obj) {
        if (obj.hasOwnProperty(a)) {
            parts.push(`${a}:${obj[a]}`);
        }
    }

    return parts.join(',');
}

function translateOption (opt) {
    if (typeof opt === 'object') {
        return toKeyVal(opt);
    }

    return opt;
}

var defaults = {
    jar: soyJarPath,
    args: {
        basedir: undefined,
        data: undefined,
        dependencies: undefined,
        glob: undefined,
        i18n: undefined,
        outdir: undefined,
        rootnamespace: undefined,
        type: 'js'
    }
};

module.exports = function (opts) {
    opts = opts || {};
    var a;
    var bin = process.env.JAVA_HOME ? `${process.env.JAVA_HOME}/bin/java` : 'java';
    var parts = [
        '-jar', (opts.jar || defaults.jar)
    ];

    for (a in defaults.args) {
        if (defaults.args.hasOwnProperty(a)) {
            var value = opts.args && opts.args[a] || defaults.args[a];

            if (value !== undefined) {
                parts.push(`--${a}`);
                parts.push(translateOption(value));
            }
        }
    }

    console.log('Running soy with the following commands:', bin, parts);

    var cmd = proc.spawnSync(bin, parts);
    console.log(String(cmd.stderr));
    return cmd.exit;
};
