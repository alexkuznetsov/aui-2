var config = {
    // Chrome latest
    chrome_latest_linux: {
        browserName: 'chrome',
        platform: 'Linux'
    },
    chrome_latest_windows: {
        browserName: 'chrome',
        platform: 'Windows 10'
    },
    chrome_latest_osx: {
        browserName: 'chrome',
        platform: 'OS X 10.11'
    },

    // Firefox latest
    firefox_latest_linux: {
        browserName: 'firefox',
        seleniumVersion: '2.48.0'
    },
    firefox_latest_windows: {
        browserName: 'firefox',
        platform: 'Windows 10'
    },
    firefox_45_osx: {
        browserName: 'firefox',
        version: '45.0',
        platform: 'OS X 10.11',
        seleniumVersion: '2.48.0'
    },

    safari_latest_osx: {
        browserName: 'safari',
        platform: 'OS X 10.11'
    },
    ie_10: {
        browserName: 'internet explorer',
        version: '10',
        platform: 'Windows 7'
    },
    ie_11: {
        browserName: 'internet explorer',
        version: '11',
        platform: 'Windows 8.1'
    },
    ie_edge_latest: {
        browserName: 'microsoftedge',
        platform: 'Windows 10'
    }
};

Object.keys(config).forEach(function (key) {
    config[key].base = 'SauceLabs';
    config[key].screenResolution = '1024x768';
});

module.exports = config;
