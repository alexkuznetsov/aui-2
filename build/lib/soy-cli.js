var soy = require('./soy');
var _ = require('underscore');

function compileArgs(options) {
    // these need to be passed in as options, not extra args
    let sanitisedExtraArgs = _.omit(options.extraArgs,
        't', 'type',
        'b', 'basedir',
        'o', 'outdir',
        'g', 'glob'
    );

    return _.extend({}, sanitisedExtraArgs, {
        type: options.type,
        basedir: options.basedir,
        outdir: options.outdir,
        glob: options.glob
    });
}

module.exports = function soyCli(opts) {
    let cmdArgParts = compileArgs(opts);

    return {
        compile: function(glob) {
            return new Promise(function(resolve, reject) {
                try {
                    soy({
                        args: Object.assign({}, cmdArgParts, {glob})
                    });
                    resolve();
                } catch (e) {
                    reject(e);
                }
            });
        }
    }
};
