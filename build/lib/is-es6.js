'use strict';

module.exports = function (file) {
    var isFileInAuiFromPackageManager = [
        '/aui/docs/src/scripts',
        '/aui/src/js/aui',
        '/aui/tests/'
    ].some(function (path) {
        return file.path.indexOf(path) > -1;
    });

    // Assume things in the following folders aren't being ES6ified.
    var isEs6 = [
        '.tmp/compiled-soy',
        'js-vendor/',
        'node_modules/'
    ].every(function (path) {
        return file.path.indexOf(path) === -1;
    });

    return isFileInAuiFromPackageManager || isEs6;
};
