'use strict';

import $ from './jquery';
import './spinner';
import amdify from './internal/amdify';
import skate from './internal/skate';

const CLASS_NOTIFICATION_INITIALISED = '_aui-form-notification-initialised';

const ATTRIBUTE_NOTIFICATION_PREFIX = 'data-aui-notification-';
const ATTRIBUTE_NOTIFICATION_WAIT = ATTRIBUTE_NOTIFICATION_PREFIX + 'wait';
const ATTRIBUTE_NOTIFICATION_INFO = ATTRIBUTE_NOTIFICATION_PREFIX + 'info';
const ATTRIBUTE_NOTIFICATION_ERROR = ATTRIBUTE_NOTIFICATION_PREFIX + 'error';
const ATTRIBUTE_NOTIFICATION_SUCCESS = ATTRIBUTE_NOTIFICATION_PREFIX + 'success';

const NOTIFICATION_PRIORITY = [
    ATTRIBUTE_NOTIFICATION_ERROR,
    ATTRIBUTE_NOTIFICATION_SUCCESS,
    ATTRIBUTE_NOTIFICATION_WAIT,
    ATTRIBUTE_NOTIFICATION_INFO
];

const MESSAGE_CLASSES = {};
MESSAGE_CLASSES[ATTRIBUTE_NOTIFICATION_INFO] = 'description';
MESSAGE_CLASSES[ATTRIBUTE_NOTIFICATION_ERROR] = 'error';


function initialiseNotification($field) {
    if (!isFieldInitialised($field)) {
        prepareFieldMarkup($field);
        synchroniseNotificationDisplay($field);
    }
}

function isFieldInitialised($field) {
    return $field.hasClass(CLASS_NOTIFICATION_INITIALISED);
}

function prepareFieldMarkup($field) {
    $field.addClass(CLASS_NOTIFICATION_INITIALISED);
    appendDescription($field);
}

function appendDescription($field, message) {
    message = message ? message : getNotificationMessage($field);
    if (getFieldNotificationType($field) === ATTRIBUTE_NOTIFICATION_INFO) {
        $field.after(`<div class='description'>${message}</div>`)
    }
}

function getNotificationMessage($field) {
    var notificationType = getFieldNotificationType($field);
    var message = notificationType ? $field.attr(notificationType) : '';
    return formatMessage(message);
}

function formatMessage(message) {
    if (message === '') {
        return message;
    }

    var messageArray = jsonToArray(message);

    if (messageArray.length === 1) {
        return messageArray[0];
    } else {
        return '<ul><li>' + messageArray.join('</li><li>') + '</li></ul>';
    }
}

function jsonToArray(jsonOrString) {
    var jsonArray;
    try {
        jsonArray = JSON.parse(jsonOrString);
    } catch (exception) {
        jsonArray = [jsonOrString];
    }
    return jsonArray;
}

function getFieldNotificationType($field) {
    var fieldNotificationType;
    NOTIFICATION_PRIORITY.some(function (prioritisedNotification) {
        if ($field.is('[' + prioritisedNotification + ']')) {
            fieldNotificationType = prioritisedNotification;
            return true;
        }
    });

    return fieldNotificationType;
}

function isFieldContainingActiveElement($field) {
    return $.contains($field[0], document.activeElement);
}

function shouldMessageBeVisible($field) {
    return (isFieldContainingActiveElement($field) || elementIsActive($field) || !focusTogglesTooltip($field));
}

function synchroniseNotificationDisplay(field) {
    const $field = $(field);

    if (!isFieldInitialised($field)) {
        return;
    }
    const type = getFieldNotificationType($field);
    const showSpinner = type === ATTRIBUTE_NOTIFICATION_WAIT;

    setFieldSpinner($field, showSpinner);

    const message = getNotificationMessage($field);
    if (shouldMessageBeVisible($field) && message) {
        if (type === ATTRIBUTE_NOTIFICATION_ERROR) {
            appendErrorMessages($field, [message]);
        } else if (type === ATTRIBUTE_NOTIFICATION_INFO) {
            appendDescription($field, message);
        }
    } else {
        getMessageContainer($field, MESSAGE_CLASSES[type]);
    }
}
function errorMessageTemplate(messages) {
    let list = messages
        .map(message => `<li><span class="aui-icon aui-icon-small aui-iconfont-error aui-icon-notification">${message}</span>${message}</li>`)
        .join('');
    return `<div class="error"><ul>${list}</ul></div>`;
}

function appendErrorMessages($field, messages) {
    let previousErrors = getMessageContainer($field, 'error');
    if (previousErrors.length > 0) {
        previousErrors.remove();
    }
    $field.after(errorMessageTemplate(messages));
}

function getMessageContainer($field, type) {
    return $field.parent().find(`.${type}`);
}


function focusTogglesTooltip($field) {
    return $field.is(':aui-focusable');
}

function elementIsActive($el) {
    const el = ($el instanceof $) ? $el[0] : $el;
    return el && el === document.activeElement;
}

function isSpinnerForFieldAlreadyExisting($field) {
    return $field.next('aui-spinner').length > 0;
}

function setFieldSpinner($field, isSpinnerVisible) {
    if (isSpinnerVisible && !isSpinnerForFieldAlreadyExisting($field)) {
        $field.after('<aui-spinner size="small"></aui-spinner>');
    } else {
        $field.parent().find('aui-spinner').remove();
    }
}

skate('data-aui-notification-field', {
    attached: function (element) {
        initialiseNotification($(element));
    },
    attributes: (function () {
        const attrs = {};
        NOTIFICATION_PRIORITY.forEach(function (type) {
            attrs[type] = synchroniseNotificationDisplay;
        });
        return attrs;
    }()),
    type: skate.type.ATTRIBUTE
});

amdify('aui/form-notification');

export {
    getMessageContainer,
    appendErrorMessages,
    errorMessageTemplate
}
