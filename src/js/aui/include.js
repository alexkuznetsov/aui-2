'use strict';

import $ from './jquery';
import { fn as deprecateFn } from './internal/deprecation';
import contains from './contains';
import globalize from './internal/globalize';

var included = [];

function include (url) {
    if (!contains(included, url)) {
        included.push(url);
        var s = document.createElement('script');
        s.src = url;
        $('body').append(s);
    }
}

var include = deprecateFn(include, 'include', {
    sinceVersion: '5.9.0',
    extraInfo: 'No alternative will be provided. Use a proper module loader instead.'
});

globalize('include', include);

export default include;
