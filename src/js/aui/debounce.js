'use strict';

import globalize from './internal/globalize';

export default function debounce (func, wait) {
    var timeout;
    var result;

    return function () {
        var args = arguments;
        var context = this;
        var later = function () {
            result = func.apply(context, args);
            context = args = null;
        };

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);

        return result;
    };
}

globalize('debounce', debounce);

export function debounceImmediate (func, wait) {
    var timeout = null;
    var result;

    return function () {
        var context = this;
        var args = arguments;
        var later = function () {
            timeout = context = args = null;
        };

        if (timeout === null) {
            result = func.apply(context, args);
        }

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);

        return result;
    };
}

globalize('debounceImmediate', debounceImmediate);
