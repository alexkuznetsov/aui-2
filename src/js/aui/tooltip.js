'use strict';

import $ from './jquery';
import '../../js-vendor/jquery/jquery.tipsy';

function handleStringOption ($self, options, stringOption) {
    // Pass string values straight to tipsy
    $self.tipsy(stringOption);

    if (stringOption === 'destroy') {
        if (options.live) {
            $($self.context).off('.tipsy', $self.selector);
        } else {
            $self.unbind('.tipsy');
        }
    }

    return $self;
}

function bindTooltip ($self, options) {
    $self.tipsy(options);

    const hideOnClick = options && options.hideOnClick && (options.trigger === 'hover' || !options.trigger && $self.tipsy.defaults.trigger === 'hover');
    if (hideOnClick) {
        const onClick = function() {
            $(this).tipsy('hide');
        };
        if (options.live) {
            $($self.context).on('click.tipsy', $self.selector, onClick);
        } else {
            $self.bind('click.tipsy', onClick);
        }
    }
    return $self;
}

$.fn.tooltip = function (options) {
    const allOptions = $.extend({}, $.fn.tooltip.defaults, options);

    // Handle live option
    if (allOptions.live) {
        if (typeof options === 'string') {
            handleStringOption(this, allOptions, options);
        } else {
            bindTooltip(this, allOptions);
        }
        return this;
    }

    // If not live, bind each object in the collection
    return this.each(function() {
        const $this = $(this);
        if (typeof options === 'string') {
            handleStringOption($this, allOptions, options);
        } else {
            bindTooltip($this, allOptions);
        }
        return $this;
    });
};

$.fn.tooltip.defaults = {
    opacity: 1.0,
    offset: 1,
    delayIn: 500,
    hoverable: true,
    hideOnClick: true,
    aria: true
};
