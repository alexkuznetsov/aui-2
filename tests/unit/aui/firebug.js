'use strict';

import {firebug, warnAboutFirebug} from '../../../src/js/aui/firebug';

describe('aui/firebug', function () {
    it('exports', function () {
        expect(firebug).to.be.a('function');
        expect(warnAboutFirebug).to.be.a('function');
    });

    it('globals', function () {
        expect(AJS.firebug.toString()).to.equal(firebug.toString());
        expect(AJS.warnAboutFirebug.toString()).to.equal(warnAboutFirebug.toString());
    });
});
