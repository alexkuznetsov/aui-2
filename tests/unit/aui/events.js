'use strict';

import * as events from '../../../src/js/aui/events';
import _ from 'underscore';

describe('aui/events', function () {
    it('global', function () {
        _.forEach(events, function(item, key) {
            expect(AJS[key].toString()).to.equal(item.toString());
        });
    });
});
