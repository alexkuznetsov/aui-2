'use strict';

import onTextResize from '../../../src/js/aui/on-text-resize';

describe('aui/on-text-resize', function () {
    it('globals', function () {
        expect(AJS.onTextResize.toString()).to.equal(onTextResize.toString());
    });
});
