'use strict';

import isVisible from '../../../src/js/aui/is-visible';

describe('aui/is-visible', function () {
    it('globals', function () {
        expect(AJS.isVisible.toString()).to.equal(isVisible.toString());
    });
});
