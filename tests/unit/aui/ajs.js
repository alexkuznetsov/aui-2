'use strict';

import '../../../src/js/aui';
import createElement from '../../../src/js/aui/create-element';

describe('aui/ajs', function () {
    it('globals', function () {
        expect(AJS).to.be.a('function');
    });

    describe('deprecated behaviour', function() {
        it('should return the result of the create-element module', function () {
            var globalResult = AJS('yo', 'adrian');
            var createResult = createElement('yo', 'adrian');
            expect(globalResult.tagName).to.equal(createResult.tagName);
            expect(globalResult.textContent).to.equal(createResult.textConent);
        });
    });

});
