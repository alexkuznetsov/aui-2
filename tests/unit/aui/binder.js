'use strict';

import Binder from '../../../src/js/aui/binder';

describe('aui/binder', function () {
    it('globals', function () {
        expect(AJS.Binder.toString()).to.equal(Binder.toString());
    });
});
