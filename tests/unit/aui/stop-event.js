'use strict';

import stopEvent from '../../../src/js/aui/stop-event';

describe('aui/stop-event', function () {
    it('globals', function () {
        expect(AJS.stopEvent.toString()).to.equal(stopEvent.toString());
    });
});
