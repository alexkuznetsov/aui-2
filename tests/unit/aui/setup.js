'use strict';

import '../../../src/js/aui/setup';

describe('aui/setup', function () {
    it('should globalize jQuery or Zepto to $', function () {
        expect(AJS.$.toString()).to.equal($.toString());
    });
});
