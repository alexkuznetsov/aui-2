AJS.$(document).ready(function() {
    var navs = ['nav1', 'nav2', 'nav3', 'nav4'];
    var numHeaders = 8;
    navs.forEach(function (navIdPrefix) {
        for (var i = 1; i <= numHeaders; i++) {
            AUITEST.newTestDropdown(navIdPrefix + '-dropdown2-header' + i);
        }
    });
    AUITEST.newTestDropdown('nav3.dropdown2.header.dots');
});
