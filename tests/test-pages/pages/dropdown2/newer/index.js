require(['jquery', './dropdown-server', './dropdown-fixtures'], function($, server, fixtures) {
    var { serverResponse, noSectionLabelResponse, opensSubmenuResponse, customAsyncTemplate } = fixtures;

    var removeServerResponse = function () {
        server.responses.forEach(function(response) {
            if (response.url.source === 'custom-async-dropdown') {
                var index = server.responses.indexOf(response);
                server.responses.splice(index, 1);
            }
        });
    };

    $(function() {
        var responseCodeInput = document.getElementById('response-code');
        var responseCodeDelay = document.getElementById('response-delay');
        var responseDataInput = document.getElementById('response-data');

        var resetButton = document.getElementById('async-reset');
        var restoreXhrButton = document.getElementById('restore-xhr');
        var form = document.getElementById('dd-custom-form');

        var toggleContainer = document.getElementById('dropdown-container');

        var applyChanges = function () {
            toggleContainer.innerHTML = customAsyncTemplate;
            server.respondWith(/custom-async-dropdown/, [
                parseInt(responseCodeInput.value),
                { 'Content-Type': 'application/json' },
                responseDataInput.value
            ]);
            server.autoRespondAfter = responseCodeDelay.value * 1000;
        };

        var handleChange = function () {
            removeServerResponse();
            applyChanges();
        };

        restoreXhrButton.addEventListener('click', function () {
            server.restore();
        });

        responseCodeInput.addEventListener('change', function() {
            document.querySelector('input[name="custom-response"][value="custom"]').checked = true;
            handleChange();
        });
        responseDataInput.addEventListener('change', handleChange);
        responseCodeDelay.addEventListener('change', handleChange);

        form.addEventListener('submit', function (e) {
            e.preventDefault();
            applyChanges();
        });

        var sampleResponseMap = {
            '1': function () { return JSON.stringify(serverResponse); },
            '2': function () { return JSON.stringify(noSectionLabelResponse); },
            '3': function () { return JSON.stringify(opensSubmenuResponse); },
            'custom': function () { return '{}'; }
        };
        var defaultResponse = function () {
            responseCodeInput.value = 200;
            responseCodeDelay.value = 10;
            document.querySelector('input[name="custom-response"]').click();
            handleChange();
        };

        $('#dd-custom-form').on('click', 'input[name="custom-response"]', function(e) {
            var selected = e.target.value;
            var action = sampleResponseMap[selected] || defaultResponse;
            responseDataInput.value = action();
            handleChange();
        });

        resetButton.addEventListener('click', defaultResponse);

        defaultResponse();
        applyChanges();
    });
});
